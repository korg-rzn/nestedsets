<?php
namespace Krg\Nestedsets;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as Hydrator;
use Krg\Nestedsets\ConfigInterface;
use Krg\Nestedsets\Config;
use Krg\Nestedsets\ManagerInterface;
use Krg\Nestedsets\EntityInterface;

class Manager implements ManagerInterface
{

	private $_debug = 0;

	/**
	 * @var ConfigInterface 
	 */
	protected $Config;

//------------------------------------------------------------------------------
	function __construct(ConfigInterface $Config)
	{
		$this->Config = $Config;
	}

//------------------------------------------------------------------------------
	/**
	 * Создаёт объект менеджера по массиву настроек
	 * 
	 * @param EntityManager $EntityManager
	 * @param array $arConfig массив настроек
	 * - entity_class: (обязательный) класс сущности doctrine
	 * - field_tree: поле сущности, заменяющее nsTree
	 * - field_parent: поле сущности, заменяющее nsParent
	 * - field_left: поле сущности, заменяющее nsLeft
	 * - field_right: поле сущности, заменяющее nsRight
	 * - field_level: поле сущности, заменяющее nsLevel
	 * - tree: ID дерева
	 * @return \self
	 */
	static function create(EntityManager $EntityManager, $arConfig){
		if(!array_key_exists('class', $arConfig)){
			throw new \Exception('Paramert "entity_class" not found in config for NsManager');
		};
		$className = $arConfig['class'];
		if(!class_exists($className)){
			throw new \Exception('Class "'.$className.'" not found');
		};
		$Config = new Config($EntityManager, $className);
		if(array_key_exists('field_tree', $arConfig))	$Config->setFieldTree($arConfig['field_tree']);
		if(array_key_exists('field_parent', $arConfig)) $Config->setFieldParent($arConfig['field_parent']);
		if(array_key_exists('field_left', $arConfig))	$Config->setFieldLeft($arConfig['field_left']);
		if(array_key_exists('field_right', $arConfig))	$Config->setFieldRight($arConfig['field_right']);
		if(array_key_exists('field_level', $arConfig))	$Config->setFieldLevel($arConfig['field_level']);
		if(array_key_exists('tree', $arConfig))			$Config->setTree($arConfig['tree']);
		
		$NSM = new self($Config);
		return $NSM;
	}
//------------------------------------------------------------------------------
	function getConfig(): ConfigInterface{
		return $this->Config;
	}
//------------------------------------------------------------------------------
	function createElement($arData = []): EntityInterface{
		$Hydr = new Hydrator($this->Config->getEntityManager());
		$entityClass = $this->Config->getEntityClass();
		$Element = new $entityClass;
		$Hydr->hydrate($arData, $Element);
		return $Element;
	}
//------------------------------------------------------------------------------
	function duplicate(EntityInterface $Element, $pLast = false):EntityInterface{
		$EM = $this->Config->getEntityManager();
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		if (!$this->_inTree($Element)) {
																				$this->__debug('Елемент не в дереве');
			throw new \Exception('Element not in tree');
		};
																				$this->__debug('Елемент в дереве');
																				$this->__debug('Element left: '.$Element->getNsLeft().'; right: '.$Element->getNsRight());
		//	Параметры ROOT-элемента (элемент, указаный для дублирования)
		$ELeft = $Element->getNsLeft();
		$ERight = $Element->getNsRight();
		$ELevel = $Element->getNsLevel();
		$EParent = $Element->getNsParent();
		$EParentObject = $this->_getElementObject($EParent);
		//var_dump($EParentObject); die();
		$rP = $pLast?$this->getLastChild($EParentObject)->getNsRight():$ERight;
																				$this->__debug('$rP: '.$rP);
		$indDiff = $rP - $ELeft + 1;	//	
		//	Сдвигаем left и right следующих элементов
		$this->_changeNsLeftByRanges([[$ERight-$ELeft+1,$rP+1]]);
		$this->_changeNsRightByRanges([[$ERight-$ELeft+1,$rP+1]]);
		//	Копируем элемент со всеми дочерними
		$QB->andWhere($alias . '.' . $fLeft . ' >= :start');
		$QB->setParameter('start', $ELeft);
		$QB->andWhere($alias . '.' . $fRight . ' <= :end');
		$QB->setParameter('end', $ERight);
		$aRes = $QB->getQuery()->getResult();
		$Parents = [];	//	Массив для сохранени родителей разных уровней
		$NewForReturn = null;
		/* @var $iRes EntityInterface */
		foreach ($aRes as $iRes) {
																				$this->__debug($iRes->getName());
			$level = $iRes->getNsLevel();	//	Уровень элемента
			$levelDif = $level - $ELevel;	//	Разница уровней текущего элемента и 
																				$this->__debug('- Level: '.$level.', dif: '.$levelDif);
			$Parents[$levelDif] = $iRes;	
			//$this->__debug($iRes->getName());
			$New = clone $iRes;
			$New->setId(NULL);
			//	Родитель для текущего элемента
			if(isset($Parents[$levelDif-1])){
				$Parent = $Parents[$levelDif-1];
				if(!$this->Config->isParentAssociation()){
					$Parent = $Parent->getId();
				};
			}else{
				$Parent = $EParent;	
			};
			$New->setNsParent($Parent);
			$New->setNsLeft($New->getNsLeft() + $indDiff);
			$New->setNsRight($New->getNsRight() + $indDiff);
																				$this->__debug('- Left: '.$New->getNsLeft().', Right: '.$New->getNsRight());
			$EM->persist($New);
			if(!$NewForReturn)
				$NewForReturn = $New;
		};
		
		$EM->flush();
		return $NewForReturn;
	}
//------------------------------------------------------------------------------
	public function to(EntityInterface $Elem, $RefElement = false, $toLast = false)
	{
		$EM = $this->Config->getEntityManager();

		if ($this->_inTree($Elem)) {  //	Существует
																				$this->__debug('Изменить');
			$Tree = $Elem->getNsTree();
			$eL = $Elem->getNsLeft();
			$eR = $Elem->getNsRight();
																				$this->__debug('Element l: ' . $eL . ' r: ' . $eR);

			$rP = false; //	Опрная точка
			$rS = 0; //	Левая граница первого промежуточного элемента
			$rE = 0; //	Левая граница последнего промежуточного элемента
			$eK = 0; //	Коэф-т для перемещаемых
			$rK = 0; //	Коэф-т для промежуточных

			if ($RefElement) {
																				$this->__debug('Опорный элемент указан');
				$RefElement = $this->_getElementObject($RefElement);
				if ($RefElement->getNsTree() != $Elem->getNsTree()) {
					throw new \Exception('Different trees');
				};
				if ($this->_isInner($RefElement, $Elem)) {
					throw new \Exception('Moving after inner element');
				};
				$RefElementParent = $this->_getElementObject($RefElement->getNsParent());
				$Elem->setNsParent((!$this->Config->isParentAssociation() && $RefElementParent) ? $RefElementParent->getId() : $RefElementParent);
				$rP = $RefElement->getNsRight();
				$level = $RefElement->getNsLevel();
			} else {
				$ParentTo = $this->_getElementObject($Elem->getNsParent());
				if ($ParentTo && $ParentTo->getNsTree() != $Elem->getNsTree()) {
					throw new \Exception('Different trees');
				};
				if ($ParentTo) {
					if ($this->_isInner($ParentTo, $Elem)) {
						throw new \Exception('Moving into inner element');
					};
				};
				$Elem->setNsParent((!$this->Config->isParentAssociation() && $ParentTo) ? $ParentTo->getId() : $ParentTo);
				$level = $ParentTo ? ($ParentTo->getNsLevel() + 1) : 1;
																				$this->__debug('To parent: ' . ($ParentTo ? $ParentTo->getId() : 'NULL'));
				$ParentFrom = $this->_parentByNs($Elem);
																				$this->__debug('From parent: ' . ($ParentFrom ? $ParentFrom->getId() : 'NULL'));
				if ($RefElement === null) {
																				$this->__debug('Опорный элемент NULL (сделать первым)');
					$rP = $ParentTo ? $ParentTo->getNsLeft() : 0;
				} else {
																				$this->__debug('Опорный элемент не указан');
					//	Ищем текущего родителя
					if ($ParentTo != $ParentFrom) {
																				$this->__debug('Сменить родителя');
						if ($ParentTo) {
							$rP = $ParentTo->getNsRight() - 1;
						} elseif ($LastChild = $this->getLastChild()) { //	Последний дочерний элемент
							$rP = $LastChild->getNsRight();
						} else {
							$rP = 0;
						};
					} elseif ($toLast) { //	$ParentTo==$ParentFrom
																				$this->__debug('Переместить в конец');
						if ($LastChild = $this->getLastChild($ParentTo)) {
							if ($Elem->getId() != $LastChild->getId()) {
								$rP = $LastChild->getNsRight();
							};
						};
					} else {
																				$this->__debug('Ничего не менять');
					};
				};
			};
			$this->_changeLevelWithChildren($Elem, $level);
			$EM->persist($Elem);
			if ($rP !== false) {
																				$this->__debug('rP: ' . $rP);
				if ($eR < $rP) { //	вниз
																				$this->__debug('Перемещение вниз');
					$eK = $rP - $eR;
					$rK = $eL - $eR - 1;
					$rS = $eR + 1;
					$rE = $rP;
				} else { //	вверх
																				$this->__debug('Перемещение вверх');
					$eK = $rP + 1 - $eL;
					$rK = $eR - $eL + 1;
					$rS = $rP + 1;
					$rE = $eL - 1;
				};

																				$this->__debug('Перемещаемые: ' . $eL . ' ... ' . $eR . ' : ' . $eK);
																				$this->__debug('Промежуточные: ' . $rS . ' ... ' . $rE . ' : ' . $rK);
				$this->_changeNsLeftByRanges([[$eK,$eL,$eR],[$rK,$rS,$rE]]);
				$this->_changeNsRightByRanges([[$eK,$eL,$eR],[$rK,$rS,$rE]]);
			};
		} else {  //	Добавить
																				$this->__debug('Добавить');
			$rP = 0; //	Опрная точка
			$Tree = $this->Config->getTree();
			if ($RefElement) {
																				$this->__debug('Опорный элемент указан');
				$RefElement = $this->_getElementObject($RefElement);
				$Parent = $this->_getElementObject($RefElement->getNsParent());
				$Tree = $RefElement->getNsTree();
				$rP = $RefElement->getNsRight();
			} else {
				$Parent = $this->_getElementObject($Elem->getNsParent());
																				$this->__debug('Parent: ' . ($Parent ? $Parent->getId() : 'NULL'));
				if ($Parent) {
					$Tree = $Parent->getNsTree();
				};
				$rP = $Parent ? $Parent->getNsLeft() : 0;
				if ($RefElement === null) {
																				$this->__debug('Опорный элемент NULL (сделать первым)');
				} else {
																				$this->__debug('Опорный элемент не указан (сделать последним)');
					if ($LastChild = $this->getLastChild($Parent)) {
						$rP = $LastChild->getNsRight();
					};
				};
			};
																				$this->__debug('rP: ' . $rP);
			$eL = $rP + 1;
			$eR = $eL + 1;
			$Elem->setNsTree($Tree);
			$Elem->setNsParent((!$this->Config->isParentAssociation() && $Parent) ? $Parent->getId() : $Parent);
			$Elem->setNsLeft($eL);
			$Elem->setNsRight($eR);
			$Elem->setNsLevel($Parent ? ($Parent->getNsLevel() + 1) : 1);
			$EM->persist($Elem);
//			//	Промежуточные
			$this->_changeNsLeftByRanges([[2, $rP + 1]]);
			$this->_changeNsRightByRanges([[2, $rP + 1]]);
		};
		$EM->flush();
	}

//------------------------------------------------------------------------------
	public function getTree($levels = false)
	{
		return $this->getDescendants(null, $levels);
	}

//------------------------------------------------------------------------------
	function getLastChild(?EntityInterface $Elem = null)
	{
		$fParent = $this->Config->getFieldParent();
		$fRight = $this->Config->getFieldRight();
		$fTree = $this->Config->getFieldTree();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();

		if ($Elem) {
			$QB->andWhere($alias . '.' . $fParent . ' = :parent');
			$QB->setParameter('parent', $Elem->getId());
		} else {
			$QB->andWhere($alias . '.' . $fParent . ' IS NULL');
		};
		$QB->orderBy($alias . '.' . $fRight, 'desc');
		$QB->setMaxResults(1);
		$aRes = $QB->getQuery()->getResult();
		return $aRes ? $aRes[0] : null;
	}

//------------------------------------------------------------------------------
	public function getFirstChild(?EntityInterface $Elem = null)
	{
		$fParent = $this->Config->getFieldParent();
		$fLeft = $this->Config->getFieldLeft();
		$fTree = $this->Config->getFieldTree();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();

		if ($Elem) {
			$QB->andWhere($alias . '.' . $fParent . ' = :parent');
			$QB->setParameter('parent', $Elem->getId());
		} else {
			$QB->andWhere($alias . '.' . $fParent . ' IS NULL');
		};
		$QB->setMaxResults(1);
		$aRes = $QB->getQuery()->getResult();
		return $aRes ? $aRes[0] : null;
	}

//------------------------------------------------------------------------------
	public function getAncestors(EntityInterface $Elem)
	{
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$fTree = $this->Config->getFieldTree();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		
		$QB->andWhere($alias . '.' . $fLeft . ' < :start');
		$QB->setParameter('start', $Elem->getNsLeft());
		$QB->andWhere($alias . '.' . $fRight . ' > :end');
		$QB->setParameter('end', $Elem->getNsRight());
		
		return $QB->getQuery()->getResult();
	}

//------------------------------------------------------------------------------
	public function getChildren(?EntityInterface $Elem = null)
	{
		$fParent = $this->Config->getFieldParent();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		
		if ($Elem) {
			$QB->andWhere($alias . '.' . $fParent . ' = :parent');
			$QB->setParameter('parent', $Elem->getId());
		} else {
			$QB->andWhere($alias . '.' . $fParent . ' IS NULL');
		};
		return $QB->getQuery()->getResult();
	}

//------------------------------------------------------------------------------
	public function getDescendants(?EntityInterface $Elem = null, $levels = false)
	{
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$fLevel = $this->Config->getFieldLevel();

		$EM = $this->Config->getEntityManager();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		
		if ($Elem) {
			$eS = $Elem->getNsLeft();
			$eE = $Elem->getNsRight();
		}else{
			$eS = $eE = false;
		};
		if($eS){
			$QB->andWhere($alias . '.' . $fLeft . ' > :start');
			$QB->setParameter('start', $eS);
		};
		if($eE){
			$QB->andWhere($alias . '.' . $fRight . ' < :end');
			$QB->setParameter('end', $eE);
		};
		if($levels > 0){
			if($Elem)
				$levels = $Elem->getNsLevel() + $levels;
			$QB->andWhere($alias . '.' . $fLevel . ' <= :level');
			$QB->setParameter('level', $levels);
		};
		return $QB->getQuery()->getResult();
	}
//------------------------------------------------------------------------------
	public function getNext(EntityInterface $Element): ?EntityInterface
	{
		$fLeft = $this->Config->getFieldLeft();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		$QB->andWhere($alias . '.' . $fLeft.' = :left');
		$QB->setParameter('left', $Element->getNsRight() + 1);
		$dbRes = $QB->getQuery()->getResult();
		if(!empty($dbRes))
			return $dbRes[0];
		return null;
	}

//------------------------------------------------------------------------------
	public function getPrevious(EntityInterface $Element): ?EntityInterface
	{
		$fRight = $this->Config->getFieldRight();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		$QB->andWhere($alias . '.' . $fRight.' = :right');
		$QB->setParameter('right', $Element->getNsLeft() - 1);
		$dbRes = $QB->getQuery()->getResult();
		if(!empty($dbRes))
			return $dbRes[0];
		return null;
	}

//------------------------------------------------------------------------------
	public function remove(EntityInterface $Elem)
	{
																				$this->__debug('Удалить');
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$fTree = $this->Config->getFieldTree();
		
		$EM = $this->Config->getEntityManager();
		$entityClass = $this->Config->getEntityClass();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		if ($this->_inTree($Elem)) {  //	Существует
																				$this->__debug('Елемент существует');
			$eL = $Elem->getNsLeft();
			$eR = $Elem->getNsRight();
			$Tree = $Elem->getNsTree();
																				$this->__debug('L: '.$eL.' R: '.$eR);
			//	Удаление
			$QB->andWhere($alias . '.' . $fLeft . ' >= :left');
			$QB->setParameter('left', $eL);
			$QB->andWhere($alias . '.' . $fRight . ' <= :right');
			$QB->setParameter('right', $eR);
			$aRes = $QB->getQuery()->getResult();
			/* @var $iRes EntityInterface */
			foreach ($aRes as $iRes) {
				$EM->remove($iRes);
			};
			//	Обновление после стоящих
			$d = -($eR - $eL + 1);
			$this->_changeNsLeftByRanges([[$d, $eR + 1]]);
			$this->_changeNsRightByRanges([[$d, $eR + 1]]);
			$EM->flush();
		};
	}
//------------------------------------------------------------------------------
	public function check(bool $debug = false, bool $repair = false)
	{
		$TreeElements = $this->getTree();
		$arElementsByParent = [];
		foreach($TreeElements as $Element){
			$parent = $Element->getNsParent();
			if($parent && is_object($parent)){
				$parent = $parent->getId();
			};
			$arElementsByParent[$parent][$Element->getId()] = $Element;
		};
		$res = $this->_check($arElementsByParent, $repair);
		if($repair && !$res['status']){
			$this->Config->getEntityManager()->flush();
		};
		if(!empty($arElementsByParent)){
			$res['status'] = true;
			$res['messages'][] = 'Raw tree elements';
		};
		return $debug?$res:$res['status'];
	}
//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	private function _check(&$arElementsByParent, bool $repair, $parentId = null, &$border = false, $level = 1){
		$status = true;
		$arMess = [];
		if($border ===false) $border = 0;
		if(isset($arElementsByParent[$parentId])){
			$Elements = $arElementsByParent[$parentId];
			unset($arElementsByParent[$parentId]);
			foreach($Elements as $Elem){
				if($Elem->getNsLevel()!=$level){
					$status = false;
					$arMess[] = 'Nested Sets Error: element "'.$Elem->getId().'": nsLevel = '.$Elem->getNsLevel().'; expected '.$level;
					if($repair){
						$Elem->setNsLevel($level);
						$this->Config->getEntityManager()->persist($Elem);
						$arMess[] = '* Repaired *';
					};
				};
				$border++;
				if($Elem->getNsLeft()!=$border){
					$status = false;
					$arMess[] = 'Nested Sets Error: element "'.$Elem->getId().'": nsLeft = '.$Elem->getNsLeft().'; expected '.$border;
					if($repair){
						$Elem->setNsLeft($border);
						$this->Config->getEntityManager()->persist($Elem);
						$arMess[] = '* Repaired *';
					};
				};
				$subRes = $this->_check($arElementsByParent, $repair, $Elem->getId(), $border, $level + 1);
				if(!$subRes['status']){
					$status = false;
				};
				$arMess = array_merge($arMess, $subRes['messages']);
				$border++;
				if($Elem->getNsRight()!=$border){
					$status = false;
					$arMess[] = 'Nested Sets Error: element "'.$Elem->getId().'": nsRight = '.$Elem->getNsRight().'; expected '.$border;
					if($repair){
						$Elem->setNsRight($border);
						$this->Config->getEntityManager()->persist($Elem);
						$arMess[] = '* Repaired *';
					};
				};
			};
		};
		return [
			'status'=>$status,
			'messages'=>$arMess,
		];
	}

////////////////////////////////////////////////////////////////////////////////

	/**
	 * Проверяет является ли один элемент потомком другого
	 * @param EntityInterface $Elem Проверяемый элемент
	 * @param EntityInterface $Parent Патенциальный родитель
	 */
	private function _isInner(EntityInterface $Elem, EntityInterface $Parent)
	{
		return ($Elem->getNsLeft() > $Parent->getNsLeft() && $Elem->getNsRight() < $Parent->getNsRight());
	}
//------------------------------------------------------------------------------

	/**
	 * Получить родителя по NsLeft и NsRight
	 * @param EntityInterface $Elem
	 * @return EntityInterface|null
	 */
	private function _parentByNs(EntityInterface $Elem)
	{
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$fTree = $this->Config->getFieldTree();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();

		$eL = $Elem->getNsLeft();
		$eR = $Elem->getNsRight();

		$QB->andWhere($alias . '.' . ($fLeft) . ' < :left');
		$QB->andWhere($alias . '.' . ($fRight) . ' > :right');
		$QB->setParameter('left', $eL);
		$QB->setParameter('right', $eR);
		$QB->orderBy($alias . '.' . $fLeft, 'desc');
		$aRes = $QB->getQuery()->getResult();
		return $aRes ? $aRes[0] : null;
	}
//------------------------------------------------------------------------------
	/**
	 * Проверяет, пренадлежит ли элемент дереву текущего менеджера
	 * 
	 * @param EntityInterface $Element
	 * @return boolean
	 */
	private function _inTree(EntityInterface $Element){
		if (!($id = $Element->getId()))
			return false;
		$EM = $this->Config->getEntityManager();
		if(!$EM->contains($Element)) return false;				//	Проверка отслеживания элемента Doctrine менеджером
		$treeClass = $this->Config->getEntityClass();
		if(!($Element instanceof $treeClass)) return false;	//	Проверка соответствия класса
		if($this->Config->getTree() != $Element->getNsTree()) return false;	//	Проверка соответсвия дерева
		if(!$EM->find($treeClass, $id)) return false;		//	Проверка наличия елемента в БД
		return true;
	}
//------------------------------------------------------------------------------
	/**
	 * Устанавливает новый уровень вложенности элементу и всем его детям (в соовеоствии с подуровнями)
	 * @param EntityInterface $Elem Элемент, которому надо установить новый уровень
	 * @param type $level Новый уровень
	 */
	private function _changeLevelWithChildren(EntityInterface $Elem, $level)
	{
		$levelOld = $Elem->getNsLevel();
		$levelDif = $level - $levelOld;
		if ($levelDif == 0)
			return;
		//	Поля 
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$fTree = $this->Config->getFieldTree();
		//	Объекты БД
		$EM = $this->Config->getEntityManager();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		
		$eL = $Elem->getNsLeft();
		$eR = $Elem->getNsRight();
		
		$QB->andWhere($alias . '.' . $fLeft . ' >= :left');
		$QB->setParameter('left', $eL);
		$QB->andWhere($alias . '.' . $fRight . ' <= :right');
		$QB->setParameter('right', $eR);
		$aRes = $QB->getQuery()->getResult();
		//	Oбновление
		/* @var $iRes EntityInterface */
		foreach ($aRes as $iRes) {
			$iRes->setNsLevel($iRes->getNsLevel() + $levelDif);
			$EM->persist($iRes);
		};
	}

//------------------------------------------------------------------------------
	private function _changeNsLeftByRanges($ranges){
		$this->__changeNsBorderByRanges($ranges, true);
	}
	
//------------------------------------------------------------------------------
	private function _changeNsRightByRanges($ranges){
		$this->__changeNsBorderByRanges($ranges, false);
	}

//------------------------------------------------------------------------------
	/**
	 * UPDATE каждого элемента, но за одну транзакцию.
	 * 
	 * @param type $Tree
	 * @param array $ranges массив массивов с параметрами: 0 - dif, 1 - start, 2 - end (false по умол. - до конца)
	 * @param bool $nsBorder
	 * @throws \Exception
	 */
	private function __changeNsBorderByRanges($ranges, bool $nsBorder){
		if(!is_array($ranges)){
			throw new \Exception('Ranges is not array');
		};
		$fLeft = $this->Config->getFieldLeft();
		$fRight = $this->Config->getFieldRight();
		$fTree = $this->Config->getFieldTree();
		$QB = $this->Config->getQueryBuilder();
		$alias = $this->Config->getQueryBuilderAlias();
		$EM = $this->Config->getEntityManager();
		
		$fBorder = $nsBorder?$fLeft:$fRight;
		
		$QBCond = [];
		foreach($ranges as $rKey => &$r){
			$r = $this->rangeNormalize($r);
			if(!$r) continue;
			$cond = '('.$alias.'.'.$fBorder.' >= :start_'.$rKey.')';
			$QB->setParameter('start_'.$rKey, $r['s']);
			if($r['e']!==false){
				$cond .= ' and ('.$alias.'.'.$fBorder.' <= :end_'.$rKey.')';
				$QB->setParameter('end_'.$rKey, $r['e']);
			};
			$QBCond[] = $cond;
		};	
		unset($r);
		$QB->andWhere('('.implode(' OR ', $QBCond).')');
		$aRes = $QB->getQuery()->getResult();
		//	Oбновление
		/* @var $iRes EntityInterface */
		foreach ($aRes as $iRes) {
			$aply = false;
			foreach($ranges as $rKey => $r){
				$borderValue = $nsBorder?$iRes->getNsLeft():$iRes->getNsRight();
				if($r['s']>$borderValue) {
					continue;
				};
				if($r['e'] && $r['e']<$borderValue){
					continue;
				};
				if($nsBorder){
					$iRes->setNsLeft($borderValue + $r['d']);
				}else{
					$iRes->setNsRight($borderValue + $r['d']);
				};
				$aply = true;
				break;
			};
			if($aply){
				$EM->persist($iRes);
			};
		};
	}

//------------------------------------------------------------------------------
	private function rangeNormalize($r){
		if(!is_array($r)){
			throw new \Exception('Range is not array');
		};
		if(count($r)<2){
			throw new \Exception('Range array count < 2. Must 2 or 3 (diff, start, end=false)');
		};
		if ($r[1] <= 0) {
			throw new \Exception('Krg\Core error: start less or equal 0');
		};
		if (isset($r[2]) && $r[2] < 0) {
			throw new \Exception('Krg\Core error: end less 0');
		};
		$_r = [
			'd' => $r[0],
			's' => $r[1],
			'e' => $r[2]??false,
		];
		
		if ($_r['d'] == 0 || $_r['s'] == $_r['e']){
			return false;
		};
		if ($_r['e'] > 0 && $_r['s'] > $_r['e']) {	//	Если значения start>end, меняем эти значения местами
			$_t = $_r['s'];
			$_r['s'] = $_r['e'];
			$_r['e'] = $_t;
			unset($_t);
		};
		return $_r;
	}

//------------------------------------------------------------------------------

	/**
	 * Получает объект элемета дерева не зависимо от того, был ли передан объект или его ID
	 * Если по ID элемент не найден, вернётся null
	 * 
	 * @param EntityInterface|string|null $Element
	 * @return EntityInterface|null Объект элемента
	 * @throws \Exception
	 */
	private function _getElementObject($Element)
	{
		if (!$Element)
			return null;
		$manEntityClass = $this->Config->getEntityClass();
		$EM = $this->Config->getEntityManager();
		if (!is_object($Element)) {   //	
			$Element = $EM->find($manEntityClass, $Element);
			if (!$Element) {
				throw new \Exception('Krg\Core error: parent not found');
			};
		};
		if (!($Element instanceof $manEntityClass)) {
			throw new \Exception('Krg\Core error: class of parent no suppurt class manager');
		};
		return $Element;
	}

//------------------------------------------------------------------------------
	private function __debug($mes, $type = false, $level = 1)
	{
		if ($this->_debug)
			echo $mes."\n";
	}

//------------------------------------------------------------------------------
	
}
