<?php
namespace Krg\Nestedsets;

use Krg\Nestedsets\ConfigInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class Config implements ConfigInterface
{
	/**
	 * @var EntityManagerInterface
	 */
	protected $EntityManager;
	protected $QueryBuilder;
	protected $entityClass;
	protected $fieldLeft = 'nsLeft';
	protected $fieldRight = 'nsRight';
	protected $fieldParent = 'nsParent';
	protected $fieldTree = 'nsTree';
	protected $fieldLevel = 'nsLevel';
	protected $tree;
	
	protected $isParentAssociation = false;		//	Свойтво класса с ID родителя является ассоциаицией

//------------------------------------------------------------------------------
	public function __construct(EntityManagerInterface $EntityManager, string $entityClass)
	{
		$this->EntityManager = $EntityManager;
		$this->entityClass = $entityClass;
		$this->detectParentAssociation();
	}
//------------------------------------------------------------------------------
	public function getEntityManager()
	{
		return $this->EntityManager;
	}

//------------------------------------------------------------------------------
	public function setFieldLeft(string $fieldName)
	{
		$this->fieldLeft = $fieldName;
		return $this;
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function getFieldLeft()
	{
		return $this->fieldLeft;
	}

//------------------------------------------------------------------------------
	public function setFieldParent(string $fieldName)
	{
		$this->fieldParent = $fieldName;
		$this->detectParentAssociation();
		return $this;
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function getFieldParent()
	{
		return $this->fieldParent;
	}

//------------------------------------------------------------------------------
	public function setFieldRight(string $fieldName)
	{
		$this->fieldRight = $fieldName;
		return $this;
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function getFieldRight()
	{
		return $this->fieldRight;
	}

//------------------------------------------------------------------------------
	public function setFieldTree(string $fieldName)
	{
		$this->fieldTree = $fieldName;
		return $this;
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function getFieldTree()
	{
		return $this->fieldTree;
	}
//------------------------------------------------------------------------------
	public function getFieldLevel()
	{
		return $this->fieldLevel;
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function setFieldLevel(string $fieldName)
	{
		$this->fieldLevel = $fieldName;
		return $this;
	}

//------------------------------------------------------------------------------
	public function setTree($tree = null)
	{
		$this->tree = $tree;
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function getTree()
	{
		$metaData = $this->EntityManager->getClassMetadata($this->entityClass);
		if($data = $metaData->associationMappings[$this->fieldTree]??null){
			//var_dump($data);
			if($this->tree && !is_object($this->tree)){
				$EntityClassName = $data['targetEntity'];
				$_tree = $this->EntityManager->find($EntityClassName, $this->tree);
				if(!$_tree){
					throw new \Exception('Tree "'.$this->tree.'" not found in entity "'.$EntityClassName.'"');
				};
				$this->tree = $_tree;
			};
		};
		return $this->tree;
	}

//------------------------------------------------------------------------------
	public function getQueryBuilder()
	{
		return $this->QueryBuilder?:$this->getQuerybuilderDefault();
	}

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
	public function setQueryBuilder(?QueryBuilder $QB = null)
	{
		$this->QueryBuilder = $QB;
	}
//------------------------------------------------------------------------------
	public function getEntityClass()
	{
		return $this->entityClass;
	}
//------------------------------------------------------------------------------
	public function getQueryBuilderAlias()
    {
        return $this->getQueryBuilder()->getRootAlias();
    }
//------------------------------------------------------------------------------
	function isParentAssociation()
	{
		return $this->isParentAssociation;
	}
//------------------------------------------------------------------------------
	function isTreeAssociation()
	{
		return $this->isTreeAssociation;
	}

////////////////////////////////////////////////////////////////////////////////
	protected function getQuerybuilderDefault(){
		/* @var $QB \Doctrine\ORM\QueryBuilder */
		$QB = $this->EntityManager->createQueryBuilder();
		$alias = 'e';
		$fTree = $this->getFieldTree();
		$QB->select($alias)
            ->from($this->getEntityClass(), $alias)
			->orderBy($alias.'.'.$this->getFieldLeft(), 'asc');
		if($Tree = $this->getTree()){
			$QB->where($alias . '.' . $fTree . ' = :tree');
			$QB->setParameter('tree', $Tree);
		}else{
			$QB->where($alias . '.' . $fTree . ' IS NULL');
		};		
		return $QB;
	}
//------------------------------------------------------------------------------
	protected function detectParentAssociation(){
		$metaData = $this->EntityManager->getClassMetadata($this->entityClass);
		$this->isParentAssociation = isset($metaData->associationMappings[$this->fieldParent]);
	}
//------------------------------------------------------------------------------
}
