<?php
namespace Krg\Nestedsets;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

interface ConfigInterface{
	/**
	 * @return \Doctrine\ORM\EntityManagerInterface
	 */
	function getEntityManager();
	function getEntityClass();
	
	function setFieldLeft(string $fieldName);
	function setFieldRight(string $fieldName);
	function setFieldParent(string $fieldName);
	function setFieldTree(string $fieldName);
	function setFieldLevel(string $fieldName);
	
	function getFieldLeft();
	function getFieldRight();
	function getFieldParent();
	function getFieldTree();
	function getFieldLevel();
	
	function setTree($tree = null);
	function getTree();

	/**
	 * @return Doctrine\ORM\QueryBuilder
	 */
	function getQueryBuilder();
	function setQueryBuilder(?QueryBuilder $QB = null);
	function getQueryBuilderAlias();
	
	function isParentAssociation();
	//function isTreeAssociation();

}
