<?php
namespace Krg\Nestedsets;

interface EntityInterface{
	function getId();
	function getNsLeft();
	function getNsRight();
	function getNsTree();
	function getNsParent();
	function getNsLevel();
	
	function setId($id);
	function setNsLeft($nsLeft);
	function setNsRight($nsRight);
	function setNsParent($nsParent);
	function setNsTree($nsTree);
	function setNsLevel($nsLevel);
}
