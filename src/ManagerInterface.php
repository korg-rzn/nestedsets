<?php
namespace Krg\Nestedsets;

use Krg\Nestedsets\EntityInterface;
use Krg\Nestedsets\ConfigInterface;

interface ManagerInterface{
	
	/**
	 * Сохранение (добавление или изменение) элемента. 
	 * Добавление нового элемента происходит в указанный в менеджере tree. 
	 * Если не указан $RefElementAfter (или false), то добавить в конец.
	 * Если $RefElementAfter = null, то добавить в начало.
	 * Если $RefElementAfter - существующий элемент (или ID элемента), то добавить новый после существующего.
	 * При изменении если $RefElementAfter не указан (или false) и родитель не менялся, то происходит, просто пересохранение элемента.
	 * Если $RefElementAfter = null, то переместить в начало.
	 * 
	 * В менеджере указывается класс сущности и дерево. Если какой-либо элемент (или ID) не будет соответствовать этим данным, будет ошибка.
	 * Если будет попытка переместить элемент в его дочерний элемент или после него, то будет ошибка.
	 * 
	 * @param EntityInterface $Elem Сохраняемый элемент. parent указывается вручную. Left, Right, Tree, Level - вычисляются
	 * @param EntityInterface|string|null|false $RefElement Опорный элемент, после которого надо разместить элемент
	 * @param boolean $toLast перемещать ли в конец, если не указан $RefElementAfter, и не изменён родитель
	 */
	function to(EntityInterface $Elem, $RefElement = false, $toLast = false);
	
	/**
	 * Возвращает массив элементов (Node) дерева
	 * @param string $tree Идентификатор дерева
	 * @param integer|boolean $levels Максимальное кол-во уровней вложенности
	 * @return array_of_Node Массив элементов (Node) дерева
	 */
	function getTree($levels = false);
	
	/**
	 * Получить последний дочерний элемент
	 * @param EntityInterface $Elem
	 * @return EntityInterface
	 */
	function getLastChild(?EntityInterface $Elem = null);
	/**
	 * Получить первый дочерний элемент
	 * @param EntityInterface $Elem
	 * @return EntityInterface
	 */
	function getFirstChild(?EntityInterface $Elem = null);
	/**
	 * Получить прямых потомков
	 * @param EntityInterface $Elem
	 * @return array_of_EntityInterface
	 */
	function getChildren(?EntityInterface $Elem = null);
	/**
	 * Получить всех потомков
	 * @param EntityInterface $Elem
	 * @return array_of_EntityInterface
	 */
	function getDescendants(?EntityInterface $Elem = null, $levels = false);
	/**
	 * Получить всех родителей
	 * @param EntityInterface $Elem
	 * @return array_of_EntityInterface
	 */
	function getAncestors(EntityInterface $Elem);
	/**
	 * Удалить элемент и всех дочерних
	 * @param EntityInterface $Elem
	 */
	function remove(EntityInterface $Elem);
	/**
	 * Проверка состояния дерева. Проверяются nsLeft, nsRight, nsLevel на соответствие nsParent. Проверяется конкретное дерево nsTree.
	 * @param bool $debug Если false, то возвращается статус проверки true|false. Если true, то возвращается ассоциативный массив с ключами:
	 * - status: статус проверки
	 * - messages: сообщения о ходе проверки. Массив 
	 *		- 0: (true|false) статус сообщения; 
	 *		- 1: текст сообщения.
	 * @param bool $repair Если true, то будет проведено восстановление nsLeft, nsRight, nsLevel в соответствии nsParent
	 */
	function check(bool $debug = false, bool $repair = false);

	/**
	 * Возвращает объект конфига
	 * @return ConfigInterface
	 */
	function getConfig(): ConfigInterface;
	
	function createElement($arData = []): EntityInterface;
	
	/**
	 * Дублирует элемент со всеми дочерними и возвращает новый элемент
	 * @param EntityInterface $Element
	 * @param boolean $last
	 * @return EntityInterface
	 */
	function duplicate(EntityInterface $Element, $last = false):EntityInterface;
	
	/**
	 * Возвращает следущий смежный элемент
	 * @param EntityInterface $Element
	 * @return EntityInterface|null
	 */
	function getNext(EntityInterface $Element): ?EntityInterface;
	
	/**
	 * Возвращает предыдущий смежный элемент
	 * @param EntityInterface $Element
	 * @return EntityInterface|null
	 */
	function getPrevious(EntityInterface $Element): ?EntityInterface;
}
