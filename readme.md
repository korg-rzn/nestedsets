#Описание
Расширение Doctrine для работы с древовидными структурами данных.
Сохранение объектов сущностей doctrine осуществляется не напрямую
через EntityManager, а через **NsManager** (Krg\Nestedsets\Manager), который
автоматически вычисляет значения всех необходимый для реализации технологии 
NestedSets полей. Так же NsManager позволяет сделать ряд наиболее
востребованых для деревьев выборок.

###Особенности:###

* ненужен единственный корневой элемент, в одном дереве может быть несколько корневых элементов;
* в одной таблице может храниться несколько деревьев.
* каждый элемент хранят идентификатор родителя, левый и правый ключи, уровень вложенности, идентификатор дерева.

#Установка
> composer require krg/doctrine-nestedsets

#Создание сущности Doctrine для хранения дерева

Нужно создать сущность для доктрины которая будет реализовывать интрефейс
`Krg\Nestedsets\EntityInterface`. Например:
```
use Doctrine\ORM\Mapping as ORM;
use Krg\Nestedsets\EntityInterface;
/**
 * @ORM\Entity
 * @ORM\Table(name="db_table")
 * @ORM\HasLifecycleCallbacks
 */
class EntityClass implements EntityInterface
{
    /**
     * @var string
     * 
     * @ORM\Column(name="Id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;
	
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
	/**
     * @ORM\Column(name="NsLeft", type="integer")
     */
    protected $nsLeft;

    /**
     * @ORM\Column(name="NsRight", type="integer")
     */
    protected $nsRight;
	
    /**
     * @ORM\Column(name="NsParent", type="string", length=36, nullable=true)
     */
    protected $nsParent;
	
    /**
     * @ORM\Column(name="NsTree", type="string", length=36, nullable=true)
     */
    protected $nsTree;

	/**
     * @ORM\Column(name="NsLevel", type="integer")
     */
    protected $nsLevel;

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	
	function getId()
	{
		return $this->id;
	}

	function setId($id)
	{
		$this->id = $id;
	}
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
	
//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	
	//	NestedSets
	function getNsLeft()
	{
		return $this->nsLeft;
	}

	function setNsLeft($nsLeft)
	{
		$this->nsLeft = $nsLeft;
	}

	function getNsRight()
	{
		return $this->nsRight;
	}

	function setNsRight($nsRight)
	{
		$this->nsRight = $nsRight;
	}

	function getNsParent()
	{
		return $this->nsParent;
	}

	function setNsParent($nsParent)
	{
		$this->nsParent = $nsParent;
	}

	function getNsTree()
	{
		return $this->nsTree;
	}

	function setNsTree($nsTree)
	{
		$this->nsTree = $nsTree;
	}

	function getNsLevel()
	{
		return $this->nsLevel;
	}

	function setNsLevel($nsLevel)
	{
		$this->nsLevel = $nsLevel;
	}
//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	
	/**
     * @ORM\PrePersist
     */
    public function createUUID()
    {
        if (!$this->getId()) {
            $this->setId((string)Uuid::uuid4());
        }
    }

	/**
     * @ORM\PrePersist
     */
    public function createNs()
    {
        if (!$this->getNsLevel()) {
            $this->setNsLevel(0);
        }
        if (!$this->getNsLeft()) {
            $this->setNsLeft(0);
        }
        if (!$this->getNsRight()) {
            $this->setNsRight(0);
        }
    }

}
```
В классе сущности должны быть следующе поля:

* **Id** - Первичный ключ, основной идентификатор. В примере используется формат UUID, но по сути это может быть любой тип данных.
* **nsParent** - идентификатор родительского элемента. Тип должен совпадать с полем Id, либо должен быть ассоциацией ManyToOne на это поле. Может быть NULL (если элемент корневой).
* **nsTree** - идентификатор дерева. Может быть простым полем или ассоциацией ManyToOne 
на иднетификатор другой сущности. Может быть NULL; в этом случае NULL так же будет 
являться иднетификатором дерева. Более того NULL будет идентификатором по умолчанию.
* **nsLeft** - левый ключ NestedSets. Тип: integer. Не может быть NULL.
* **nsRight** - правый ключ NestedSets. Тип: integer. Не может быть NULL.
* **nsLevel** - уровень вложенности элемента. Тип: integer. Не может быть NULL.

Id должен быть заполнен в ручную, либо средствами dctrine (как в примере выше).

Поля nsTree, nsLeft, nsRight, nsLevel не задаются напрямую. Они задаётся через NsManager.

Для кадого из полей должны быть обязательно заданый геттеры и сеттеры:

* getId и setId;
* getNsLeft и setNsLeft;
* getNsRight и setNsRight;
* getNsParent и setNsParent;
* getNsTree и setNsTree;
* getNsLevel и setNsLevel.

В дополнение к этим геттерам и сеттерам могут быть и другие сеттеры и сеттеры полей.

> *Например, если nsTree является ссылкой на Id пользователя, может потребоваться 
метод getUser.*

Названия полей могут быть другими. В этом случае их надо переопределить с помощью
следующих методов объекта конфига Ns-менеджера *(Krg\Nestedsets\ConfigInterface)*:

* setFieldParent
* setFieldTree
* setFieldLeft
* setFieldRight
* setFieldLevel

###Наследование от абстрактного супер-класса

```
use Doctrine\ORM\Mapping as ORM;
use Krg\Nestedsets\AbstractEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="db_table")
 * @ORM\HasLifecycleCallbacks
 */
class EntityClass extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
//------------------------------------------------------------------------------
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
}
```

Все необходимые поля, а также геттеры и сеттеры объявлены в родительком классе 
**Krg\Nestedsets\AbstractEntity**. Требуется только определить дополнительные поля,
а так же, если требуется, переопределить поля и методы родительского класса.

```
use Doctrine\ORM\Mapping as ORM;
use Krg\Nestedsets\AbstractEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="db_table")
 * @ORM\HasLifecycleCallbacks
 */
class EntityClass extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
    /**
     * @var \Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="User_id", referencedColumnName="Id", onDelete="CASCADE", nullable=true)
     * })
     */
    protected $user;

	//	Переопределяем свойство nsTree без аннотаций, чтобы исключить создание соответсвующей колонки в БД.
	protected $nsTree;
//------------------------------------------------------------------------------
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
	function getNsTree()
	{
		return $this->user;
	}

	function setNsTree($nsTree)
	{
		$this->user = $nsTree;
	}

}
//...
$Config = new \Krg\Nestedsets\Config($EntityManager, \Entity\EntityClass::class);
$Config->setFieldTree('user');
//...
```

#Использование

Создаём конфиг менеджера:

```
//	На входе менеджер сущностей Doctrine и названия класса сущности, с которым будем работать.
$Config = new \Krg\Nestedsets\Config($EntityManager, \Entity\EntityClass::class);

//	Указываем идентификатор дерева
$Config->setTree('tree-001');		//	Строковый идентификатор
$Config->setTree($User);			//	Объект класса другой сущности, если свойство идентификатора дерева является ассоциацией

//	Если требуется переопределяем названия свойств
$Config->setFieldParent('nsTreeNew');
$Config->setFieldTree('nsParentNew');
$Config->setFieldLeft('nsLeftNew');
$Config->setFieldRight('nsRightNew');
$Config->setFieldLevel('nsLevelNew');
```

Далее создаём сам NsManager:

```
$NsManager = new \Krg\NestedSets\Manager($Config);
```

###Создаём NsManager по конфигу в виде массива

```
$arConfig = [
	'class'			=> \Entity\EntityClass::class,	//	Обязательное
	'tree'			=> 'tree-001',
	'field_tree'	=> 'nsTreeNew',
	'field_parent'	=> 'nsParentNew',
	'field_left'	=> 'nsLeftNew',
	'field_right'	=> 'nsRightNew',
	'field_level'	=> 'nsLevelNew',
];
$NsManager = Manager::create($EntityManager, $arConfig);
```

###Добавление нового элемента

Элемент будет добавлен в конец списка

```
$Element = new \Entity\EntityClass();
$Element->setName('element 1');
$Element->setNsParent($ParentElement);	//	Если требуется поместить в родитеский элемент
$NsManager->to($Element);
```
или
```
$Element = $NsManager->createElement([
	'name'		=> 'element 1',
	'nsParent'	=> $ParentElement,
]);
$NsManager->to($Element);
```

#### Добавление в начало

```
$NsManager->to($Element, NULL);
```

#### Добавление после элемента

```
$NsManager->to($Element, $RefElement);
```

В данном случае родительский элемент будет переопределён.

###Перемещение элемента

Перемещение выполняется с помощью того же метода **to**.
Но есть особенность: перемещение в начало происходит передачей NULL в качестве 
второго параметра, а в конец передачей значения **true** в качетсве третьего 
параметра метода **to**:

```
$Element = $EntityManager->find(\Entity\EntityClass::class, '<идентификатор элемента>');
$Element->setName('new name');
$NsManager->to($Element);				//	Только изменение имени
$NsManager->to($Element, null);			//	Изменение имени и перемещение в начало
$NsManager->to($Element, false, true);	//	Изменение имени и перемещение в конец
```

###Удаление элемента

Удаляется элемент и все его потомки. 

```
$NsManager->remove($Element);
```

> Чтобы исключить удаление потомков, требуется их заранее переместить.

###Выборки

```
$NsManager->getTree();						//	Все элементы дерева
$NsManager->getTree(2);						//	Элементы дерева максимум 2-го уровня
$NsManager->getLastChild();					//	Последний корневой элемент
$NsManager->getLastChild($Element);			//	Последний дочерний элемент
$NsManager->getFirstChild();				//	Первый корневой элемент
$NsManager->getFirstChild($Element);		//	Первый дочерний элемент
$NsManager->getChildren();					//	Элементы первого уровня
$NsManager->getChildren($Element);			//	Прямые потомки
$NsManager->getDescendants();				//	Все потомки корня (т.е. все эелменты дерева)
$NsManager->getDescendants($Element);		//	Все потомки элемента
$NsManager->getDescendants($Element, 2);	//	Все потомки элемента максимум 2-го уровня вложенности
$NsManager->getAncestors($Element);			//	Все родители элемента начиная со старшего
```

###Дублирование ветки

Происходит копирование указанного элемента, а так же всех дочерних элементов.

```
$NsManager->duplicate($Element);		//	Сделать копию и поместить её после указанного элемента
$NsManager->duplicate($Element, true);	//	Сделать копию и поместить её в конец текущего родителя
```

#Проверка дерева

Проверка осуществляется на соответвие полей nsLeft, nsRight, nsLevel и поля nsParent

```
$NsManager->check();			//	Вернёт статус проверки
$NsManager->check(true);		//	Вернёт ас.массив: status - статус проверки, messages - массив сообщений лога проверки
$NsManager->check(true, true);	//	Выполнит проверку, восстанолвение (если потребуется) основываясь на nsParent, и вернёт статус всей операции
```

#Тестирование

Требуется создать БД и прописать найстройки для соединения в файл 
> test/config/db.php

Запустить выполнение миграций БД. Будут созданы таблицы.
> composer migrations-migrate

Запустить тесты:
> vendor/bin/phpunit test

*Иногда при тестировании вылезает подобная ошибка:*
> Doctrine\Common\Proxy\AbstractProxyFactory::getProxyDefinition(): Failed opening required '/tmp/__CG__TestEntityTest3.php' (include_path='.:/usr/share/php') in /var/www/nestedsets/vendor/doctrine/common/lib/Doctrine/Common/Proxy/AbstractProxyFactory.php on line 206

*В этом случае надо запусить следующую команду:*
>vendor/bin/doctrine orm:generate-proxies