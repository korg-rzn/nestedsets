<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190810223806 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE test1 (Id VARCHAR(36) NOT NULL, Name VARCHAR(255) NOT NULL, NsLeft INT NOT NULL, NsRight INT NOT NULL, NsParent VARCHAR(36) DEFAULT NULL, NsTree VARCHAR(36) DEFAULT NULL, NsLevel INT NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test3 (Name VARCHAR(255) NOT NULL, Id VARCHAR(36) NOT NULL, NsLeft INT NOT NULL, NsRight INT NOT NULL, NsLevel INT NOT NULL, NsParent VARCHAR(36) DEFAULT NULL, Tree_id VARCHAR(36) DEFAULT NULL, INDEX IDX_64BCBDCEF5501DA (NsParent), INDEX IDX_64BCBDCEC8365D3E (Tree_id), PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tree (Id VARCHAR(36) NOT NULL, Name VARCHAR(255) NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test2 (Name VARCHAR(255) NOT NULL, Id VARCHAR(36) NOT NULL, NsLeft INT NOT NULL, NsRight INT NOT NULL, NsParent VARCHAR(36) DEFAULT NULL, NsTree VARCHAR(36) DEFAULT NULL, NsLevel INT NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE test3 ADD CONSTRAINT FK_64BCBDCEF5501DA FOREIGN KEY (NsParent) REFERENCES test3 (Id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE test3 ADD CONSTRAINT FK_64BCBDCEC8365D3E FOREIGN KEY (Tree_id) REFERENCES tree (Id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE test3 DROP FOREIGN KEY FK_64BCBDCEF5501DA');
        $this->addSql('ALTER TABLE test3 DROP FOREIGN KEY FK_64BCBDCEC8365D3E');
        $this->addSql('DROP TABLE test1');
        $this->addSql('DROP TABLE test3');
        $this->addSql('DROP TABLE tree');
        $this->addSql('DROP TABLE test2');
    }
}
