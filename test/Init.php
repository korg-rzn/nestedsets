<?php

namespace Test;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Test\Entity\Tree as ETree;
use Test\Entity\Test1 as ETest1;
use Test\Entity\Test2 as ETest2;
use Test\Entity\Test3 as ETest3;

class Init{
	static protected $instance;
	protected $EM;
//------------------------------------------------------------------------------
	static function getInstance():self{
		if(self::$instance===null){
			self::$instance = new self();
		}
		return self::$instance;
	}
//------------------------------------------------------------------------------
	function getEntityManager(): EntityManager{
		if (!$this->EM) {
			$paths = include __DIR__.'/config/entity_paths.php';
			$config = Setup::createAnnotationMetadataConfiguration($paths);
			$driver = new AnnotationDriver(new AnnotationReader(), $paths);
			//AnnotationRegistry::registerLoader('class_exists');
			$config->setMetadataDriverImpl($driver);
			$this->EM = EntityManager::create(include __DIR__.'/config/db.php', $config);
		};
		return $this->EM;
	}
//------------------------------------------------------------------------------
	function fixture(){
		$EM = $this->getEntityManager();
		$EM->clear();
		//	Отчистка
		$EM->createQueryBuilder()->delete(ETree::class)->getQuery()->execute();
		$EM->createQueryBuilder()->delete(ETest1::class)->getQuery()->execute();
		$EM->createQueryBuilder()->delete(ETest2::class)->getQuery()->execute();
		$EM->createQueryBuilder()->delete(ETest3::class)->getQuery()->execute();
		//	Заполнение тестовыми данными
		for($i=1; $i<=3; $i++){
			$ElemName = 'Elem'.$i;
			$$ElemName = new ETree;
			$$ElemName->setId('00000000-0000-0001-0000-00000000000'.$i);
			$$ElemName->setName('Tree '.$i);
			$EM->persist($$ElemName);
		};
		$elems = [
			[				// 1
				[1,1,1],		//	2 3
				1,				//	2
				[				//	2
					[1,1,1,1],		//	2 4
				],
			],
			[				//	1
				1,1,			//	2
				[				//	2
					1,1,			//	3
					[1],			//	3 4
					1				//	3
				],
				1,				//	2
			],
			1,1,			//	1
			[[[[1]]]],		//	1 2 3 4 5
		];
		
		$treeIndex = 1;
		
		$this->_createFixtureElement(ETest1::class, null, $elems, $treeIndex++);
		$this->_createFixtureElement(ETest1::class, 'tree1', $elems, $treeIndex++);
		$this->_createFixtureElement(ETest1::class, 'tree2', $elems, $treeIndex++);
		
		$this->_createFixtureElement(ETest2::class, null, $elems, $treeIndex++);
		$this->_createFixtureElement(ETest2::class, 'tree1', $elems, $treeIndex++);
		$this->_createFixtureElement(ETest2::class, 'tree2', $elems, $treeIndex++);
		
		$this->_createFixtureElement(ETest3::class, null, $elems, $treeIndex++);
		$this->_createFixtureElement(ETest3::class, $Elem1, $elems, $treeIndex++);
		$this->_createFixtureElement(ETest3::class, $Elem2, $elems, $treeIndex++);
		
		$EM->flush();
		$EM->clear();
	}
//------------------------------------------------------------------------------
	private function _createFixtureElement($class, $tree, $elems = [], $treeIndex = 0, $parent = null, $indexes = [], &$i = false){
		$EM = $this->getEntityManager();
		$metaData = $EM->getClassMetadata($class);
		$isParentAssociation = isset($metaData->associationMappings['nsParent']);
		$ar = [];
		if($i===false){
			$i = 0;
		};
		foreach($elems as $k => $e){
			$i++;
			$_indexes = array_merge($indexes, [$k + 1]);
			$E = new $class();
			$ar['_'.implode('-', $_indexes)] = $E;
			$E->setId('00000000-0000-0002-'. str_pad((string)$treeIndex, 4, '0').'-'. str_pad(implode('', $_indexes), 12,'0'));
			$E->setName('e_'.implode('-', $_indexes));
			$E->setNsParent(!$isParentAssociation && is_object($parent)?(string)$parent->getId():$parent);
			//$E->setNsParent($parent);
			$E->setNsTree($tree);
			$E->setNsLeft($i);
			$E->setNsLevel(count($_indexes));
			$EM->persist($E);
			if(is_array($e)){
				$ar = array_merge($ar, $this->_createFixtureElement($class, $tree, $e, $treeIndex, $E, $_indexes, $i));
			};
			$i++;
			$E->setNsRight($i);
			$EM->persist($E);
		};
		return $ar;
	}
}