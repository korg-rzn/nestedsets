<?php
include __DIR__ . '/../../vendor/autoload.php';

// bootstrap.php
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Test\Init;

$EM = Init::getInstance()->getEntityManager();

$platform = $EM->getConnection()->getDatabasePlatform();
$platform->registerDoctrineTypeMapping('enum', 'string');

AnnotationRegistry::registerLoader('class_exists');

return ConsoleRunner::createHelperSet($EM);

