<?php

return [
    'migrations_directory' => __DIR__. '/../migrations',
    'name' => 'Migrations Name',
    'migrations_namespace' => 'DoctrineMigrations',
    'table_name' => 'migrations_table',     
];
