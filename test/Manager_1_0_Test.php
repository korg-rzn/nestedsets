<?php

namespace Test;

use PHPUnit\Framework\TestCase;
use Krg\Nestedsets\Config;
use Krg\Nestedsets\Manager;
use Test\Init;
use Test\Entity\Tree as ETree;
use Test\Entity\Test1 as ETest1;
//use Test\Entity\Test2 as ETest2;
//use Test\Entity\Test3 as ETest3;

class Manager_1_0_Test extends TestCase {

	protected $indexTree = 1;
	protected $className = ETest1::class;
	protected $tree = null;
	protected $NSM;

//------------------------------------------------------------------------------
	protected function createNSM():Manager {
		if ($this->NSM === null) {
			$EM = Init::getInstance()->getEntityManager();
			$Config = new Config($EM, $this->className);
			$Config->setTree($this->tree);
			$this->NSM = new Manager($Config);
		};
		return $this->NSM;
	}

//------------------------------------------------------------------------------
	function setUp(): void {
		Init::getInstance()->fixture();
		parent::setUp();
	}

//------------------------------------------------------------------------------
	protected function getElementId($id) {
		return '00000000-0000-0002-' . str_pad($this->indexTree, 4, '0') . '-' . str_pad(str_replace('-', '', $id), 12, '0');
	}

//------------------------------------------------------------------------------
	protected function assertElement($id, $left, $right, $level) {
		$Elem = Init::getInstance()->getEntityManager()->find($this->className, $this->getElementId($id));
		$this->assertNotEmpty($Elem);
		$this->assertEquals($left, $Elem->getNsLeft());
		$this->assertEquals($right, $Elem->getNsRight());
		$this->assertEquals($level, $Elem->getNsLevel());
	}

//------------------------------------------------------------------------------
	// 01 Переместить не последний в конец в корне
	/**
	 * @group test-01-01
	 * @group
	 */
	function testFunc1() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$iElem = $Rep->find($this->getElementId('2'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, false, true);
		
		//	Проверка
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('2', 39, 58, 1);
		$this->assertElement('3', 25, 26, 1);
		$this->assertElement('4', 27, 28, 1);
		$this->assertElement('5', 29, 38, 1);
	}

	// 02 Переместить последний в конец в корне (не перемещать)
	/**
	 * @group test-01-02
	 * @group 
	 */
	function testFunc2() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$iElem = $Rep->find($this->getElementId('5'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, false, true);
		
		//	Проверка
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('2', 25, 44, 1);
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('4', 47, 48, 1);
		$this->assertElement('5', 49, 58, 1);
	}

	//	03 Переместить не последний в конец в элементе
	/**
	 * @group test-01-03
	 * @group 
	 */
	function testFunc03() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('1-3-1-2'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, false, true);
		
		//	Проверка
		$this->assertElement('1-3-1', 13, 22, 3);
		$this->assertElement('1-3-1-2', 20, 21, 4);
		$this->assertElement('1-3-1-1', 14, 15, 4);
		$this->assertElement('1-3-1-3', 16, 17, 4);
		$this->assertElement('1-3-1-4', 18, 19, 4);
	}

//	04 Переместить последний в конец в элементе (ничего)
	/**
	 * @group test-01-04
	 * @group 
	 */
	function testFunc04() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('1-3-1-4'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, false, true);
		
		//	Проверка
		$this->assertElement('1-3-1', 13, 22, 3);
		$this->assertElement('1-3-1-1', 14, 15, 4);
		$this->assertElement('1-3-1-2', 16, 17, 4);
		$this->assertElement('1-3-1-3', 18, 19, 4);
		$this->assertElement('1-3-1-4', 20, 21, 4);
	}

//	05 Переместить вниз из элемента в элемент
	/**
	 * @group test-01-05
	 * @group 
	 */
	function testFunc05() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('1-3-1-4'));
		$iElem->setNsParent($Rep->find($this->getElementId('5-1')));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('1-3-1-4', 55, 56, 3);
		$this->assertElement('1-3-1-3', 18, 19, 4);
		$this->assertElement('1-3-1', 13, 20, 3);
		$this->assertElement('1-3', 12, 21, 2);
		$this->assertElement('1', 1, 22, 1);
		$this->assertElement('2', 23, 42, 1);
		$this->assertElement('5-1', 48, 57, 2);
		$this->assertElement('5-1-1', 49, 54, 3);
		$this->assertElement('5', 47, 58, 1);
	}

//	06 Переместить вниз из корня в элемент
	/**
	 * @group test-01-06
	 * @group 
	 */
	function testFunc06() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('4'));
		$iElem->setName('Edited');
		$iElem->setNsParent($Rep->find($this->getElementId('5-1')));
		$this->createNSM()->to($iElem);
		//	Проверка
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('4', 55, 56, 3);
		$this->assertElement('5', 47, 58, 1);
		$this->assertElement('5-1', 48, 57, 2);
		$this->assertElement('5-1-1', 49, 54, 3);
		$this->assertElement('5-1-1-1', 50, 53, 4);
	}

//	07 Переместить вниз из элемента в конень
	/**
	 * @group test-01-07
	 * @group 
	 */
	function testFunc07() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setName('Edited');
		$iElem->setNsParent(null);
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('2-3-3', 55, 58, 1);
		$this->assertElement('2-3-3-1', 56, 57, 2);
		$this->assertElement('2', 25, 40, 1);
		$this->assertElement('2-3', 30, 37, 2);
		$this->assertElement('2-3-2', 33, 34, 3);
		$this->assertElement('2-3-4', 35, 36, 3);
		$this->assertElement('5', 45, 54, 1);
	}

//	08 Переместить вверх из элемента в элемент
	/**
	 * @group test-01-08
	 * @group 
	 */
	function testFunc08() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setNsParent($Rep->find($this->getElementId('1-1')));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('2-3-3', 9, 12, 3);
		$this->assertElement('2-3-3-1', 10, 11, 4);
		$this->assertElement('1', 1, 28, 1);
		$this->assertElement('1-1', 2, 13, 2);
		$this->assertElement('1-1-1', 3, 4, 3);
		$this->assertElement('1-2', 14, 15, 2);
		$this->assertElement('1-3-1', 17, 26, 3);
		$this->assertElement('2', 29, 44, 1);
		$this->assertElement('2-1', 30, 31, 2);
		$this->assertElement('2-3', 34, 41, 2);
		$this->assertElement('2-3-1', 35, 36, 3);
		$this->assertElement('2-3-4', 39, 40, 3);
	}

//	09 Переместить вверх из корня в элемент
	/**
	 * @group test-01-09
	 * @group 
	 */
	function testFunc09() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('5'));
		$iElem->setName('Edited');
		$iElem->setNsParent($Rep->find($this->getElementId('1-1')));
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('5', 9, 18, 3);
		$this->assertElement('5-1', 10, 17, 4);
		$this->assertElement('5-1-1', 11, 16, 5);
		$this->assertElement('5-1-1-1', 12, 15, 6);
		$this->assertElement('1', 1, 34, 1);
		$this->assertElement('1-3', 22, 33, 2);
		$this->assertElement('2', 35, 54, 1);
		$this->assertElement('3', 55, 56, 1);
		$this->assertElement('4', 57, 58, 1);
	}

//	10 Изменить без перемещения в элементе
	/**
	 * @group test-01-10
	 * @group 
	 */
	function testFunc10() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('1-3-1-4'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('1-3-1-4', 20, 21, 4);
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('2', 25, 44, 1);
	}

//	11 Изменить без перемещения в корне
	/**
	 * @group test-01-11
	 * @group 
	 */
	function testFunc11() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('4'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('4', 47, 48, 1);
		$this->assertElement('5', 49, 58, 1);
	}

//	12 Переместить вниз из элемента в элемент в начало
	/**
	 * @group test-01-12
	 * @group 
	 */
	function testFunc12() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		//	Изменить с опорным NULL (переместить в начало) с родителем после
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setNsParent($Rep->find($this->getElementId('5-1')));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('2-3-3', 47, 50, 3);
		$this->assertElement('2-3-3-1', 48, 49, 4);
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('2', 25, 40, 1);
		$this->assertElement('3', 41, 42, 1);
		$this->assertElement('4', 43, 44, 1);
		$this->assertElement('5', 45, 58, 1);
		$this->assertElement('5-1', 46, 57, 2);
		$this->assertElement('5-1-1', 51, 56, 3);
		$this->assertElement('5-1-1-1', 52, 55, 4);
	}

//	13 Переместить вниз из корня в элемент в начало
	/**
	 * @group test-01-13
	 * @group 
	 */
	function testFunc13() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('4'));
		$iElem->setName('Edited');
		$iElem->setNsParent($Rep->find($this->getElementId('5-1')));
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('4', 49, 50, 3);
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('5', 47, 58, 1);
		$this->assertElement('5-1', 48, 57, 2);
		$this->assertElement('5-1-1', 51, 56, 3);
	}

//	14 Переместить вверх из элемента в элемент в начало
	/**
	 * @group test-01-14
	 * @group 
	 */
	function testFunc14() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setNsParent($Rep->find($this->getElementId('1-1')));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('2-3-3', 3, 6, 3);
		$this->assertElement('2-3-3-1', 4, 5, 4);
		$this->assertElement('1', 1, 28, 1);
		$this->assertElement('1-1', 2, 13, 2);
		$this->assertElement('1-1-1', 7, 8, 3);
		$this->assertElement('1-2', 14, 15, 2);
		$this->assertElement('1-3-1', 17, 26, 3);
		$this->assertElement('2', 29, 44, 1);
		$this->assertElement('2-1', 30, 31, 2);
		$this->assertElement('2-3', 34, 41, 2);
		$this->assertElement('2-3-1', 35, 36, 3);
		$this->assertElement('2-3-4', 39, 40, 3);
	}

//	15 Переместить вверх из элемента в корень в начало
	/**
	 * @group test-01-15
	 * @group 
	 */
	function testFunc15() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setName('Edited');
		$iElem->setNsParent(null);
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('2-3-3', 1, 4, 1);
		$this->assertElement('2-3-3-1', 2, 3, 2);
		$this->assertElement('1', 5, 28, 1);
		$this->assertElement('1-1', 6, 13, 2);
		$this->assertElement('2', 29, 44, 1);
		$this->assertElement('2-1', 30, 31, 2);
		$this->assertElement('2-3', 34, 41, 2);
		$this->assertElement('2-3-1', 35, 36, 3);
		$this->assertElement('2-3-4', 39, 40, 3);
	}

//	16 Переместить вверх из корня в элемент в начало
	/**
	 * @group test-01-16
	 * @group 
	 */
	function testFunc16() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('5'));
		$iElem->setName('Edited');
		$iElem->setNsParent($Rep->find($this->getElementId('1-1')));
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('5', 3, 12, 3);
		$this->assertElement('5-1', 4, 11, 4);
		$this->assertElement('5-1-1', 5, 10, 5);
		$this->assertElement('5-1-1-1', 6, 9, 6);
		$this->assertElement('1', 1, 34, 1);
		$this->assertElement('1-1-3', 17, 18, 3);
		$this->assertElement('1-3', 22, 33, 2);
		$this->assertElement('2', 35, 54, 1);
		$this->assertElement('3', 55, 56, 1);
		$this->assertElement('4', 57, 58, 1);
	}

//	17 Переместить в начало в элементе
	/**
	 * @group test-01-17
	 * @group 
	 */
	function testFunc17() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('2-3', 30, 41, 2);
		$this->assertElement('2-3-1', 35, 36, 3);
		$this->assertElement('2-3-2', 37, 38, 3);
		$this->assertElement('2-3-3', 31, 34, 3);
		$this->assertElement('2-3-3-1', 32, 33, 4);
		$this->assertElement('2-3-4', 39, 40, 3);
	}

//	18 Переместить в начало в корне
	/**
	 * @group test-01-18
	 * @group 
	 */
	function testFunc18() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('2', 1, 20, 1);
		$this->assertElement('2-3-3-1', 12, 13, 4);
		$this->assertElement('1', 21, 44, 1);
		$this->assertElement('1-3-1-2', 36, 37, 4);
		$this->assertElement('3', 45, 46, 1);
	}

//	19 Поместить после (вниз)
	/**
	 * @group test-01-19
	 * @group 
	 */
	function testFunc19() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		//	Изменить с опорным элементом
		$iElem = $Rep->find($this->getElementId('1-1'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, $Rep->find($this->getElementId('1-3-1-3')));
		
		//	Проверка
		$this->assertElement('1-1', 12, 19, 4);
		$this->assertElement('1-1-2', 15, 16, 5);
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('1-2', 2, 3, 2);
		$this->assertElement('1-3-1', 5, 22, 3);
		$this->assertElement('1-3-1-3', 10, 11, 4);
		$this->assertElement('1-3-1-4', 20, 21, 4);
		$this->assertElement('2', 25, 44, 1);
	}

//	20 Поместить после (вверх)
	/**
	 * @group test-01-20
	 * @group 
	 */
	function testFunc20() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3-3'));
		$iElem->setName('Edited');
		$this->createNSM()->to($iElem, $Rep->find($this->getElementId('1-3-1-3')));
		
		//	Проверка
		$this->assertElement('2-3-3', 20, 23, 4);
		$this->assertElement('2-3-3-1', 21, 22, 5);
		$this->assertElement('1', 1, 28, 1);
		$this->assertElement('1-2', 10, 11, 2);
		$this->assertElement('1-3-1', 13, 26, 3);
		$this->assertElement('1-3-1-3', 18, 19, 4);
		$this->assertElement('1-3-1-4', 24, 25, 4);
		$this->assertElement('2', 29, 44, 1);
		$this->assertElement('3', 45, 46, 1);
	}

//	21 Добавить в корень в конец
	/**
	 * @group test-01-21
	 * @group 
	 */
	function testFunc21() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('5', 49, 58, 1);
	}

//	22 Добавить в корень в начало
	/**
	 * @group test-01-22
	 * @group 
	 */
	function testFunc22() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('1', 3, 26, 1);
		$this->assertElement('3', 47, 48, 1);
		$this->assertElement('5', 51, 60, 1);
	}

//	23 Добавить в пустой элемент в конец
	/**
	 * @group test-01-23
	 * @group 
	 */
	function testFunc23() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$iElem->setNsParent($Rep->find($this->getElementId('1-3-1-3')));
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('1', 1, 26, 1);
		$this->assertElement('1-1', 2, 9, 2);
		$this->assertElement('1-3', 12, 25, 2);
		$this->assertElement('1-3-1', 13, 24, 3);
		$this->assertElement('1-3-1-1', 14, 15, 4);
		$this->assertElement('1-3-1-3', 18, 21, 4);
		$this->assertElement('1-3-1-4', 22, 23, 4);
		$this->assertElement('2', 27, 46, 1);
		$this->assertElement('3', 47, 48, 1);
		$this->assertElement('5', 51, 60, 1);
	}

//	24 Добавить в непустой элемент в конец
	/**
	 * @group test-01-24
	 * @group 
	 */
	function testFunc24() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$iElem->setNsParent($Rep->find($this->getElementId('1-3-1')));
		$this->createNSM()->to($iElem);
		
		//	Проверка
		$this->assertElement('1', 1, 26, 1);
		$this->assertElement('1-1', 2, 9, 2);
		$this->assertElement('1-3', 12, 25, 2);
		$this->assertElement('1-3-1', 13, 24, 3);
		$this->assertElement('1-3-1-1', 14, 15, 4);
		$this->assertElement('1-3-1-3', 18, 19, 4);
		$this->assertElement('1-3-1-4', 20, 21, 4);
		$this->assertElement('2', 27, 46, 1);
		$this->assertElement('3', 47, 48, 1);
		$this->assertElement('5', 51, 60, 1);
	}

//	25 Добавить в пустой элемент в начало
	/**
	 * @group test-01-25
	 * @group 
	 */
	function testFunc25() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$iElem->setNsParent($Rep->find($this->getElementId('1-3-1-3')));
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('1', 1, 26, 1);
		$this->assertElement('1-1', 2, 9, 2);
		$this->assertElement('1-3', 12, 25, 2);
		$this->assertElement('1-3-1', 13, 24, 3);
		$this->assertElement('1-3-1-1', 14, 15, 4);
		$this->assertElement('1-3-1-3', 18, 21, 4);
		$this->assertElement('1-3-1-4', 22, 23, 4);
		$this->assertElement('2', 27, 46, 1);
		$this->assertElement('3', 47, 48, 1);
		$this->assertElement('5', 51, 60, 1);
	}

//	26 Добавить в непустой элемент в начало
	/**
	 * @group test-01-26
	 * @group 
	 */
	function testFunc26() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$iElem->setNsParent($Rep->find($this->getElementId('1-3-1')));
		$this->createNSM()->to($iElem, null);
		
		//	Проверка
		$this->assertElement('1', 1, 26, 1);
		$this->assertElement('1-1', 2, 9, 2);
		$this->assertElement('1-3', 12, 25, 2);
		$this->assertElement('1-3-1', 13, 24, 3);
		$this->assertElement('1-3-1-1', 16, 17, 4);
		$this->assertElement('1-3-1-3', 20, 21, 4);
		$this->assertElement('1-3-1-4', 22, 23, 4);
		$this->assertElement('2', 27, 46, 1);
		$this->assertElement('3', 47, 48, 1);
		$this->assertElement('5', 51, 60, 1);
	}

//	27 Добавить после корневого
	/**
	 * @group test-01-27
	 * @group 
	 */
	function testFunc27() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$this->createNSM()->to($iElem, $Rep->find($this->getElementId('2')));
		
		//	Проверка
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('2', 25, 44, 1);
		$this->assertElement('3', 47, 48, 1);
		$this->assertElement('5', 51, 60, 1);
	}

//	28 Добавить после некорневого
	/**
	 * @group test-01-28
	 * @group 
	 */
	function testFunc28() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = new $this->className();
		$iElem->setName('New element');
		$this->createNSM()->to($iElem, $Rep->find($this->getElementId('1-3-1')));
		
		//	Проверка
		$this->assertElement('1', 1, 26, 1);
		$this->assertElement('1-1', 2, 9, 2);
		$this->assertElement('1-3', 12, 25, 2);
		$this->assertElement('1-3-1', 13, 22, 3);
		$this->assertElement('1-3-1-1', 14, 15, 4);
		$this->assertElement('1-3-1-4', 20, 21, 4);
		$this->assertElement('2', 27, 46, 1);
	}

//	29 Переместить во внутренний
	/**
	 * @group test-01-29
	 * @group 
	 */
	function testFunc29() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('1-3'));
		$iElem->setName('Edited');
		$iElem->setNsParent($Rep->find($this->getElementId('1-3-1-2')));
		$errors = 0;
		try {
			$this->createNSM()->to($iElem);
		} catch (\Exception $e) {
			$errors = $e->getMessage();
		};
		//	Проверка
		$this->assertNotEquals(0, $errors);
	}

//	30 Переместить после внутреннего
	/**
	 * @group test-01-30
	 * @group 
	 */
	function testFunc30() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('1-3'));
		$iElem->setName('Edited');
		$errors = 0;
		try {
			$this->createNSM()->to($iElem, $Rep->find($this->getElementId('1-3-1')));
		} catch (\Exception $e) {
			$errors = $e->getMessage();
		};
		//	Проверка
		$this->assertNotEquals(0, $errors);
	}

//	31 Удаление
	/**
	 * @group test-01-31
	 * @group 
	 */
	function testFunc31() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		//	Тело
		$iElem = $Rep->find($this->getElementId('2-3'));
		$this->createNSM()->remove($iElem);
		
		//	Проверка
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('1-1', 2, 9, 2);
		$this->assertElement('2', 25, 32, 1);
		$this->assertElement('2-1', 26, 27, 2);
		$this->assertElement('2-4', 30, 31, 2);
		$this->assertElement('3', 33, 34, 1);
	}

//	32 Получить всё дерево
	/**
	 * @group test-01-32
	 * @group 
	 */
	function testFunc32() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$tree = $this->createNSM()->getTree();
		$this->assertEquals(29, count($tree));
	}

//	33 Получить всё дерево с ограничением по уровню
	/**
	 * @group test-01-33
	 * @group 
	 */
	function testFunc33() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$tree = $this->createNSM()->getTree(2);
		$this->assertEquals(13, count($tree));
	}

//	34 Получить последнего потомка элемента
	/**
	 * @group test-01-34
	 * @group 
	 */
	function testFunc34() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Child = $this->createNSM()->getLastChild($Rep->find($this->getElementId('2-3')));
		$this->assertEquals('e_2-3-4', $Child->getName());
	}

//	35 Получить последнего потомка корня
	/**
	 * @group test-01-35
	 * @group 
	 */
	function testFunc35() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Child = $this->createNSM()->getLastChild();
		$this->assertEquals('e_5', $Child->getName());
	}

//	36 Получить первого потомка элемента
	/**
	 * @group test-01-36
	 * @group 
	 */
	function testFunc36() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Child = $this->createNSM()->getFirstChild($Rep->find($this->getElementId('2-3')));
		$this->assertEquals('e_2-3-1', $Child->getName());
	}

//	37 Получить первого потомка корня
	/**
	 * @group test-01-37
	 * @group 
	 */
	function testFunc37() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Child = $this->createNSM()->getFirstChild();
		$this->assertEquals('e_1', $Child->getName());
	}

//	38 Получить прямых потомков элемента
	/**
	 * @group test-01-38
	 * @group 
	 */
	function testFunc38() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getChildren($Rep->find($this->getElementId('2-3')));
		$this->assertEquals(4, count($Children));
	}

//	39 Получить прямых потомков корня
	/**
	 * @group test-01-39
	 * @group 
	 */
	function testFunc39() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getChildren();
		$this->assertEquals(5, count($Children));
	}

//	40 Получить всех потомков элемента
	/**
	 * @group test-01-40
	 * @group 
	 */
	function testFunc40() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getDescendants($Rep->find($this->getElementId('2-3')));
		$this->assertEquals(5, count($Children));
	}

//	41 Получить всех потомков элемента с ограничением по уровню
	/**
	 * @group test-01-41
	 * @group 
	 */
	function testFunc41() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getDescendants($Rep->find($this->getElementId('2-3')), 1);
		$this->assertEquals(4, count($Children));
	}

//	42 Получить всех потомков корня
	/**
	 * @group test-01-42
	 * @group 
	 */
	function testFunc42() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getDescendants();
		$this->assertEquals(29, count($Children));
	}

//	43 Получить всех потомков корня с ограничением по уровню
	/**
	 * @group test-01-43
	 * @group 
	 */
	function testFunc43() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getDescendants(null, 2);
		$this->assertEquals(13, count($Children));
	}

//	44 Получить всех родителей элемента
	/**
	 * @group test-01-44
	 * @group 
	 */
	function testFunc44() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Children = $this->createNSM()->getAncestors($Rep->find($this->getElementId('1-3-1')));
		$this->assertEquals(2, count($Children));
	}

//	45 Проверка дерева
	/**
	 * @group test-01-45
	 * @group 
	 */
	function testFunc45() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$res = $this->createNSM()->check(1);
		$this->assertTrue($res['status']);
	}

	//	46 Проверка дерева с ошибкой
	/**
	 * @group test-01-46
	 * @group check
	 */
	function testFunc46() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Elem = $Rep->find($this->getElementId('1-3-1'));
		$Elem->setNsLeft(16);
		$Elem->setNsRight(17);
		$Elem->setNsLevel(18);
		$EM->persist($Elem);
		$EM->flush();

		$res = $this->createNSM()->check(1, 1);
		$this->assertFalse($res['status']);

		$res = $this->createNSM()->check(1);
		$this->assertTrue($res['status']);
	}

//	47 Создать спомощью менеджера и добавить в корень в конец
	/**
	 * @group test-01-47
	 * @group 
	 */
	function testFunc47() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$NSM = $this->createNSM();
		//	Тело
		
		$iElem = $NSM->createElement([
			'name' => 'New element',
		]);
		$NSM->to($iElem);
		
		//	Проверка
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('5', 49, 58, 1);
	}

//	48 Клонировать ветку и поместить её после оригинала
	/**
	 * @group test-01-48
	 * @group 
	 */
	function testFunc48() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$NSM = $this->createNSM();
		//	Тело
		
		$iElem = $Rep->find($this->getElementId('2-3'));
		$CopyElement = $NSM->duplicate($iElem);
		
		//	Проверка
		$tree = $this->createNSM()->getTree();
		$this->assertEquals(35, count($tree));
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('3', 57, 58, 1);
		$this->assertElement('5', 61, 70, 1);
		$this->assertEquals(2, $CopyElement->getNsLevel());
		$this->assertEquals(42, $CopyElement->getNsLeft());
		$this->assertEquals(53, $CopyElement->getNsRight());
	}

//	49 Клонировать ветку и поместить её последней
	/**
	 * @group test-01-49
	 * @group 
	 */
	function testFunc49() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$NSM = $this->createNSM();
		//	Тело
		
		$iElem = $Rep->find($this->getElementId('2-3'));
		$CopyElement = $NSM->duplicate($iElem, 1);
		
		//	Проверка
		$tree = $this->createNSM()->getTree();
		$this->assertEquals(35, count($tree));
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('3', 57, 58, 1);
		$this->assertElement('5', 61, 70, 1);
		$this->assertEquals(2, $CopyElement->getNsLevel());
		$this->assertEquals(44, $CopyElement->getNsLeft());
		$this->assertEquals(55, $CopyElement->getNsRight());
	}

//	50 Клонировать корневую ветку и поместить её после оригинала
	/**
	 * @group test-01-50
	 * @group 
	 */
	function testFunc50() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$NSM = $this->createNSM();
		//	Тело
		
		$iElem = $Rep->find($this->getElementId('2'));
		$CopyElement = $NSM->duplicate($iElem);
		
		//	Проверка
		$tree = $this->createNSM()->getTree();
		$this->assertEquals(39, count($tree));
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('3', 65, 66, 1);
		$this->assertElement('5', 69, 78, 1);
		$this->assertEquals(1, $CopyElement->getNsLevel());
		$this->assertEquals(45, $CopyElement->getNsLeft());
		$this->assertEquals(64, $CopyElement->getNsRight());
	}

//	51 Клонировать корневую ветку и поместить её последней
	/**
	 * @group test-01-51
	 * @group 
	 */
	function testFunc51() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$NSM = $this->createNSM();
		//	Тело
		
		$iElem = $Rep->find($this->getElementId('2'));
		$CopyElement = $NSM->duplicate($iElem, 1);
		
		//	Проверка
		$tree = $this->createNSM()->getTree();
		$this->assertEquals(39, count($tree));
		$this->assertElement('1', 1, 24, 1);
		$this->assertElement('3', 45, 46, 1);
		$this->assertElement('5', 49, 58, 1);
		$this->assertEquals(1, $CopyElement->getNsLevel());
		$this->assertEquals(59, $CopyElement->getNsLeft());
		$this->assertEquals(78, $CopyElement->getNsRight());
	}

//	52 Получить следущий элемент
	/**
	 * @group test-01-52
	 * @group 
	 */
	function testFunc52() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Elem = $this->createNSM()->getNext($Rep->find($this->getElementId('1-1-2')));
		$this->assertEquals(7, $Elem->getNsLeft());
		$this->assertEquals(8, $Elem->getNsRight());
	}

//	53 Получить предыдущий элемент
	/**
	 * @group test-01-53
	 * @group 
	 */
	function testFunc53() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Elem = $this->createNSM()->getPrevious($Rep->find($this->getElementId('1-1-2')));
		$this->assertEquals(3, $Elem->getNsLeft());
		$this->assertEquals(4, $Elem->getNsRight());
	}

//	54 Получить следущий элемент последнего элемента
	/**
	 * @group test-01-54
	 * @group 
	 */
	function testFunc54() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Elem = $this->createNSM()->getNext($Rep->find($this->getElementId('1-1-3')));
		$this->assertNull($Elem);
	}

//	55 Получить предыдущий элемент первого элемента
	/**
	 * @group test-01-55
	 * @group 
	 */
	function testFunc55() {
		$EM = Init::getInstance()->getEntityManager();
		$Rep = $EM->getRepository($this->className);
		$Elem = $this->createNSM()->getPrevious($Rep->find($this->getElementId('1-1-1')));
		$this->assertNull($Elem);
	}

}
