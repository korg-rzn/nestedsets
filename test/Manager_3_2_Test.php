<?php
namespace Test;

use Krg\Nestedsets\Config;
use Krg\Nestedsets\Manager;
use Test\Manager_3_0_Test;
use Test\Entity\Test3 as ETest3;

class Manager_3_2_Test extends Manager_3_0_Test
{

	protected $indexTree = 9;
	protected $tree = '00000000-0000-0001-0000-000000000002';

//------------------------------------------------------------------------------
	protected function createNSM():Manager {
		
		if ($this->NSM === null) {
			$arConfig = [
				'class' => ETest3::class,
				'tree' => $this->tree,
				'field_tree' => 'customNsTree',
			];
			$this->NSM = Manager::create(Init::getInstance()->getEntityManager(), $arConfig);
			
		};
		return $this->NSM;
	}
//------------------------------------------------------------------------------
	/**
	 * @group test-09-02
	 * @group 
	 */
	function testFunc2() {
		parent::testFunc2();
	}
}
