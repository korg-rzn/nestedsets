<?php
namespace Test;

use Krg\Nestedsets\Manager;
use Test\Manager_1_0_Test;
use Test\Entity\Test3 as ETest3;

class Manager_3_0_Test extends Manager_1_0_Test
{

	protected $indexTree = 7;
	protected $className = ETest3::class;
//------------------------------------------------------------------------------
	protected function createNSM():Manager {
		$NSM = parent::createNSM();
		$Config = $NSM->getConfig();
		$Config->setFieldTree('customNsTree');
		return $NSM;
	}
//------------------------------------------------------------------------------
	/**
	 * @group test-07-02
	 * @group 
	 */
	function testFunc2() {
		parent::testFunc2();
	}

}
