<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tree
 *
 * @ORM\Entity
 * @ORM\Table(name="tree", options={"collate"="utf8_unicode_ci", "charset"="utf8"})
 */
class Tree
{
    /**
     * @var string
     * 
     * @ORM\Column(name="Id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;
	
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
//------------------------------------------------------------------------------
	function getId()
	{
		return $this->id;
	}

	function setId($id)
	{
		$this->id = $id;
	}
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
}

