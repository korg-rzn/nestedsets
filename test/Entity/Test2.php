<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Krg\Nestedsets\AbstractEntity;

/**
 * Test2
 *
 * @ORM\Entity
 * @ORM\Table(name="test2", options={"collate"="utf8_unicode_ci", "charset"="utf8"})
 * @ORM\HasLifecycleCallbacks
 */
class Test2 extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
//------------------------------------------------------------------------------
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
}

