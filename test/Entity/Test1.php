<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Krg\Nestedsets\EntityInterface;

/**
 * Test1
 *
 * @ORM\Entity
 * @ORM\Table(name="test1", options={"collate"="utf8_unicode_ci", "charset"="utf8"})
 * @ORM\HasLifecycleCallbacks
 */
class Test1 implements EntityInterface
{
    /**
     * @var string
     * 
     * @ORM\Column(name="Id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;
	
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
	/**
     * @ORM\Column(name="NsLeft", type="integer")
     */
    protected $nsLeft;

    /**
     * @ORM\Column(name="NsRight", type="integer")
     */
    protected $nsRight;
	
    /**
     * @ORM\Column(name="NsParent", type="string", length=36, nullable=true)
     */
    protected $nsParent;
	
    /**
     * @ORM\Column(name="NsTree", type="string", length=36, nullable=true)
     */
    protected $nsTree;

	/**
     * @ORM\Column(name="NsLevel", type="integer")
     */
    protected $nsLevel;
//------------------------------------------------------------------------------
	function getId()
	{
		return $this->id;
	}

	function setId($id)
	{
		$this->id = $id;
	}
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	
	
	/**
     * @ORM\PrePersist
     */
    public function createUUID()
    {
        if (!$this->getId()) {
            $this->setId((string)Uuid::uuid4());
        }
    }

	/**
     * @ORM\PrePersist
     */
    public function createNs()
    {
        if (!$this->getNsLevel()) {
            $this->setNsLevel(0);
        }
        if (!$this->getNsLeft()) {
            $this->setNsLeft(0);
        }
        if (!$this->getNsRight()) {
            $this->setNsRight(0);
        }
    }

//	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	
	//	NestedSets
	function getNsLeft()
	{
		return $this->nsLeft;
	}

	function setNsLeft($nsLeft)
	{
		$this->nsLeft = $nsLeft;
	}

	function getNsRight()
	{
		return $this->nsRight;
	}

	function setNsRight($nsRight)
	{
		$this->nsRight = $nsRight;
	}

	function getNsParent()
	{
		return $this->nsParent;
	}

	function setNsParent($nsParent)
	{
		$this->nsParent = $nsParent;
	}

	function getNsTree()
	{
		return $this->nsTree;
	}

	function setNsTree($nsTree)
	{
		$this->nsTree = $nsTree;
	}

	function getNsLevel()
	{
		return $this->nsLevel;
	}

	function setNsLevel($nsLevel)
	{
		$this->nsLevel = $nsLevel;
	}
}

