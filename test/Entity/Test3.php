<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Krg\Nestedsets\AbstractEntity;

/**
 * Test3
 *
 * @ORM\Entity
 * @ORM\Table(name="test3", options={"collate"="utf8_unicode_ci", "charset"="utf8"})
 * @ORM\HasLifecycleCallbacks
 */
class Test3 extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", nullable=false)
     */
	protected $name;
	
    /**
     * @var \Test\Entity\Test3
     *
     * @ORM\ManyToOne(targetEntity="Test\Entity\Test3")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NsParent", referencedColumnName="Id", onDelete="SET NULL")
     * })
     */
	protected $nsParent;
	
    /**
     * @var \Test\Entity\Tree
     *
     * @ORM\ManyToOne(targetEntity="Test\Entity\Tree")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Tree_id", referencedColumnName="Id", onDelete="CASCADE", nullable=true)
     * })
     */
	protected $customNsTree;
	protected $nsTree;
//------------------------------------------------------------------------------
	function getName()
	{
		return $this->name;
	}

	function setName($name)
	{
		$this->name = $name;
	}
	function getNsTree()
	{
		return $this->customNsTree;
	}

	function setNsTree($nsTree)
	{
		$this->customNsTree = $nsTree;
	}
}

