<?php
namespace Test;

use Test\Manager_1_0_Test;
use Test\Entity\Test2 as ETest2;

class Manager_2_0_Test extends Manager_1_0_Test
{

	protected $indexTree = 4;
	protected $className = ETest2::class;

}
